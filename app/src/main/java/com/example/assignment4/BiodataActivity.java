package com.example.assignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BiodataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biodata);

        TextView tv_name = (TextView) findViewById(R.id.tv_name);
        TextView tv_fatherName = (TextView) findViewById(R.id.tv_fatherName);
        TextView tv_dob = (TextView) findViewById(R.id.tv_dob);

        Button button = (Button) findViewById(R.id.btn_back);

        Bundle data = getIntent().getExtras();
        String name = data.getString("name");
        String fatherName = data.getString("fatherName");
        String dob = data.getString("dob");

        tv_name.setText("Name: " + name);
        tv_fatherName.setText("Father Name: " + fatherName);
        tv_dob.setText("Date of Birth: " + dob);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent(BiodataActivity.this, HomeActivity.class);
                startActivity(backIntent);
                finish();
            }
        });
    }
}