package com.example.assignment4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.security.Key;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        EditText name = (EditText) findViewById(R.id.et_name);
        EditText fatherName = (EditText) findViewById(R.id.et_fatherName);
        EditText dob = (EditText) findViewById(R.id.et_dob);

        Button submit = (Button) findViewById(R.id.btn_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, BiodataActivity.class);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("fatherName", fatherName.getText().toString());
                intent.putExtra("dob", dob.getText().toString());
                startActivity(intent);
            }
        });
    }
}